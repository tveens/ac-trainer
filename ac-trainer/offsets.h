#pragma once

// "ac_client.exe" + 0010F4F4 local player
// "ac_client.exe" + 0010F4F8 other players
// 0x00400000 base address

#define pPlayerGame 0x0050F4F4 // Static pointer to PlayerBase
#define pBotGame 0x0050F4F8 // Static pointer to PlayerBase
#define pNumPlayers 0x0050F500 // number of players
#define modelviewprojectMatrix 0x00501AE8
#define noRecoilCrosshair 0x0046370D // 3 bytes
#define noRecoil 0x00463786 // 10 bytes
#define infiniteAmmo 0x004637E9 // 2 bytes
#define infiniteNades 0x00463378 // 2 bytes
#define rapidFireMachineGun 0x004637E4 // 2 bytes
#define rapidKnife 0x00464514 // 2 bytes
#define rapidGrenade 0x00463387 // 2 bytes

// offsets
#define oAssaultAmmo 0x150
#define oSniperAmmo 0x144
#define oGrenadeAmmo 0x158
#define oEnableDualPistol 0x10C
#define oDualPistolAmmo 0x15C
#define oSinglePistolAmmo 0x13C
#define oHealth 0xF8
#define oArmor 0xFC
#define oPlayPositionXY1 0x34 
#define oPlayPositionXY2 0x38
#define oPlayPositionZ 0x3C
#define oHoriViewAngle 0x40
#define oVertViewAngle 0x44
#define oShooting 0x224 // 1 byte
#define oTeam 0x32C
#define oDeath 0x338 // 1 is death, 0 is alive
#define oName 0x225