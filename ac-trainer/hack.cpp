#include "hack.h"
#include "offsets.h"
#include "main.h"
#include "aimbot.h"

using namespace std;

Hack::Hack() : ol(playerSelf) {
	// start threat that searches for game
	game_checker = thread(&Hack::findProcess, this);
}

Hack::~Hack() {
	// notify threads to stop and wait for them to join
	stop_game_checker = true;
	stop_lock_vars = true;
	stop_aimbot = true;

	game_checker.join(); // started in constructor
	if (lock_vars.joinable()) lock_vars.join();
	if (aimbot.joinable()) aimbot.join();
	CloseHandle(acHandle);
}

void Hack::findProcess() {
	DWORD acExitCode;

	while (true) {
		if (stop_game_checker) break;

		// handle fails or process has exited
		if (!GetExitCodeProcess(acHandle, &acExitCode) || (acExitCode != STILL_ACTIVE)) {
			// make sure any deployed cheats get reset
			stop_lock_vars = true;
			stop_aimbot = true;
			if (lock_vars.joinable()) lock_vars.join();
			if (aimbot.joinable()) aimbot.join();

			if (win.getGameStatus()) win.Reset(); // prevents refreshing of GUI every second

			HWND whandle = FindWindow(NULL, "AssaultCube");
			// set the handle and Player Base address
			if (whandle != NULL) {
				DWORD process_ID;
				GetWindowThreadProcessId(whandle, &process_ID);
				acHWND = whandle;
				acHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, process_ID);
				uintptr_t playerAdd;
				ReadProcessMemory(acHandle, (LPCVOID)pPlayerGame, &playerAdd, 4, NULL);
				playerSelf.setPlayerAddress(playerAdd);
				win.setGameStatus(true);
			}
		}
		Sleep(1000);
	}
}

void Hack::startAimbot() {
	Aimbot ab(playerSelf);
	Player* target = nullptr;
	SHORT keypress; // records shiftkey
	while (true) {
		if (stop_aimbot) break;
		ab.readPlayerData();
		target = (win.getAimbotHeuristic() == 0) ? ab.getClosestEnemy() : ab.getClosestEnemyToCrosshair();
		keypress = GetAsyncKeyState(VK_LSHIFT);
		// when always on or shift key is down
		if (win.getAimbotStatus() == 1 || (keypress & 0x8000)) ab.updateAimbot(target);
		Sleep(1);
	}
}

void Hack::lockVars() {
	int leet = 1337;
	int pistols = 1;
	int offsets[] = {oAssaultAmmo, oSniperAmmo, oGrenadeAmmo, oDualPistolAmmo, oSinglePistolAmmo, oHealth, oArmor};

	while (true) {
		if (stop_lock_vars) break;
		playerSelf.setCustom(oEnableDualPistol, pistols);

		for (int offset : offsets) {
			playerSelf.setCustom(offset, leet);
		}
		Sleep(200);
	}
}

void Hack::toggleNoRecoil() {
	if (!win.getGameStatus()) return;

	if (win.getNoRecoilStatus()) {
		WriteProcessMemory(acHandle, (LPVOID) noRecoilCrosshair, "\xFF\x46\x1C", 3, NULL);
		WriteProcessMemory(acHandle, (LPVOID) noRecoil, "\x50\x8D\x4C\x24\x1C\x51\x8B\xCE\xFF\xD2", 10, NULL);
		win.setNoRecoilStatus(false);
	} else {
		WriteProcessMemory(acHandle, (LPVOID) noRecoilCrosshair, "\x90\x90\x90", 3, NULL);
		WriteProcessMemory(acHandle, (LPVOID) noRecoil, "\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90", 10, NULL);
		win.setNoRecoilStatus(true);
	}
}

void Hack::toggleRapidFire() {
	if (!win.getGameStatus()) return;

	if (win.getRapidFireStatus()) {
		WriteProcessMemory(acHandle, (LPVOID) rapidFireMachineGun, "\x89\x0A", 2, NULL);
		WriteProcessMemory(acHandle, (LPVOID) rapidKnife, "\x89\x0A", 2, NULL);
		WriteProcessMemory(acHandle, (LPVOID) rapidGrenade, "\x89\x0A", 2, NULL);
		win.setRapidFireStatus(false);
	} else {
		WriteProcessMemory(acHandle, (LPVOID) rapidFireMachineGun, "\x90\x90", 2, NULL);
		WriteProcessMemory(acHandle, (LPVOID) rapidKnife, "\x90\x90", 2, NULL);
		WriteProcessMemory(acHandle, (LPVOID) rapidGrenade, "\x90\x90", 2, NULL);
		win.setRapidFireStatus(true);
	}
}

void Hack::toggleInfAmmo() {
	if (!win.getGameStatus()) return;

	if (win.getInfAmmoStatus()) {
		WriteProcessMemory(acHandle, (LPVOID) infiniteAmmo, "\xFF\x0E", 2, NULL);
		WriteProcessMemory(acHandle, (LPVOID) infiniteNades, "\xFF\x0E", 2, NULL);
		win.setInfAmmoStatus(false);
	} else {
		WriteProcessMemory(acHandle, (LPVOID) infiniteAmmo, "\x90\x90", 2, NULL);
		WriteProcessMemory(acHandle, (LPVOID) infiniteNades, "\x90\x90", 2, NULL);
		win.setInfAmmoStatus(true);
	}
}

void Hack::toggleLockVars() {
	if (!win.getGameStatus()) return;

	if (win.getLockVarsStatus()) {
		stop_lock_vars = true;
		lock_vars.join();
		win.setLockVarsStatus(false);
	} else {
		stop_lock_vars = false;
		lock_vars = thread(&Hack::lockVars, this);
		win.setLockVarsStatus(true);
	}
}

void Hack::toggleAimbot() {
	if (!win.getGameStatus()) return;

	// toggle through 3 different modes, off, always on, shift only
	// status == 2 has effect in the startAimbot function
	int status = win.getAimbotStatus();
	status = (status + 1) % 3;
	if (status == 0) {
		stop_aimbot = true;
		aimbot.join();
	} else if (status == 1) {
		stop_aimbot = false;
		aimbot = thread(&Hack::startAimbot, this);
	}
	win.setAimbotStatus(status);
}

void Hack::toggleAimbotHeuristic() {
	win.toggleAimbotHeuristic();
}

void Hack::toggleOverlay() {
	if (!win.getGameStatus()) return;
	if (win.getOverlayStatus()) {
		ol.Stop();
		win.setOverlayStatus(false);
	} else {
		ol.Start();
		win.setOverlayStatus(true);
	}
}