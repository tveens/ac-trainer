#include "hack.h"
#include "main.h"

using namespace std;

HANDLE acHandle;
HWND acHWND;

int main(void) {
	Hack hack;
	RegisterHotKey(NULL, 1, 0, VK_INSERT); // insert key: quit program
	RegisterHotKey(NULL, 2, 0, VK_F7); // F7: No recoil
	RegisterHotKey(NULL, 3, 0, VK_F6); // F6: Rapid fire
	RegisterHotKey(NULL, 4, 0, VK_F5); // F5: Infinite ammo
	RegisterHotKey(NULL, 5, 0, VK_F4); // F4: Infinite health
	RegisterHotKey(NULL, 6, 0, VK_F8); // F8: Aimbot
	RegisterHotKey(NULL, 7, 0, VK_F9); // F9: Aimbot heuristic
	RegisterHotKey(NULL, 8, 0, VK_F10); // F10: Overlay
	MSG msg = { 0 };

	while (GetMessage(&msg, NULL, 0, 0) != 0) {
		if (msg.message == WM_HOTKEY) {
			switch (msg.wParam) {
			case 1:
				// exit
				PostQuitMessage(0);
				break;
			case 2:
				hack.toggleNoRecoil();
				break;
			case 3:
				hack.toggleRapidFire();
				break;
			case 4:
				hack.toggleInfAmmo();
				break;
			case 5:
				hack.toggleLockVars();
				break;
			case 6:
				hack.toggleAimbot();
				break;
			case 7:
				hack.toggleAimbotHeuristic();
				break;
			case 8:
				hack.toggleOverlay();
				break;
			}
		}
	}

	return 0;
}