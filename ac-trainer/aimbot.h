#pragma once
#include "player.h"
#include <vector>

class Aimbot {
private:
	Player& self;
	std::vector<Player> players;

public:
	Aimbot(Player&);
	void readPlayerData();
	Player* getClosestEnemy();
	Player* getClosestEnemyToCrosshair();
	void updateAimbot(Player*);
};