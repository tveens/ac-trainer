#pragma once
#include <d3d9.h>
#include <d3dx9.h>
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
#include <vector>
#include "player.h"

#define WINCLASS "Overlay"
#define CLIPPING 3.0f
#define PLAYERHEIGHT 5.0f

struct box {
	float x;
	float y;
	float width;
	float height;
	int health;
	std::vector<char> name;
};

class Overlay {
private:
	int overlayWidth = 800;
	int overlayHeight = 600;
	bool stop_overlay;
	Player &self;
	D3DXMATRIX view;
	std::vector<Player> players;

	LPDIRECT3D9 d3d;    // the pointer to our Direct3D interface
	LPDIRECT3DDEVICE9 d3ddev;     // the pointer to the device class
	ID3DXLine* d3dline;
	ID3DXFont* d3dFont;
	HINSTANCE winHandle;

	std::thread overlay;

	void initD3D(HWND);
	void renderFrame(std::vector<box> &boxes);
	void CleanD3D();
	void createWindow();
	void drawRectangle(D3DCOLOR, box &b);
	void drawFilledRectangle(D3DCOLOR, box &b, int);
	void drawName(D3DCOLOR, box &b);
	bool worldToScreen(Eigen::Vector3f, float (&o)[2]);
	void readPlayerData();
	void calculateBoxDimensions(std::vector<box> &boxes);
public:
	Overlay(Player&);
	~Overlay();
	void Start();
	void Stop();
};