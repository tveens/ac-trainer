#include <iostream>
#include <iomanip>
#include <windows.h>
#include <string>
#include "window.h"

#define WINDOWX 51
#define WINDOWY 25
static const LPCSTR _WINDOWNAME = "Assault Cube Trainer";

using namespace std;

Window::Window() {
	sGameStatus = false;
	sRapidFireStatus = false;
	sNoRecoilStatus = false;
	sInfAmmoStatus = false;
	sLockVarsStatus = false;
	sAimbotStatus = 0;
	sAimbotHeuristic = 0;
	sOverlayStatus = false;
	SetWindow();
	Print();
}

// initialize the window
void Window::SetWindow() {
	// change to desired name, size, color, screenbuffer
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTitleA(_WINDOWNAME);
	SMALL_RECT rect = { 0, 0, WINDOWX - 1, WINDOWY - 1 };
	COORD coord = { WINDOWX, WINDOWY };
	SetConsoleTextAttribute(hConsole, 0x0a);
	SetConsoleWindowInfo(hConsole, TRUE, &rect);
	SetConsoleScreenBufferSize(hConsole, coord);

	// disable resizing
	HWND winHandle = GetConsoleWindow();
	LONG currentStyle = GetWindowLongA(winHandle, GWL_STYLE);
	currentStyle ^= WS_SIZEBOX;
	currentStyle ^= WS_MAXIMIZEBOX;
	SetWindowLongA(winHandle, GWL_STYLE, currentStyle);
}

// helper function
void centerString(LPCSTR s) {
	cout << setw((WINDOWX + strlen(s)) / 2) << s << endl;
}

void Window::Print() {
	system("cls"); // ugly...
	string line = string(WINDOWX, '-') + "\n";
	cout << line << endl;
	centerString(_WINDOWNAME);
	cout << endl << line << line << endl;
	centerString("Aimbot can be toggled between always-on and");
	centerString("activate when holding shift");
	cout << endl << line << line << endl;
	cout << "GAME STATUS:           " << (sGameStatus ? "Game Found" : "Game Not Found") << "   \n"
		<< "[F4] Infinite health    -> " << (sLockVarsStatus ? "ON" : "OFF") << " <-\n"
		<< "[F5] Infinite ammo      -> " << (sInfAmmoStatus ? "ON" : "OFF") << " <-\n"
		<< "[F6] Rapid fire         -> " << (sRapidFireStatus ? "ON" : "OFF") << " <-\n"
		<< "[F7] No recoil          -> " << (sNoRecoilStatus ? "ON" : "OFF") << " <-\n"
		<< "[F8] Aimbot             -> " << (sAimbotStatus == 0 ? "OFF" : (sAimbotStatus == 1 ? "ALWAYS ON" : "HOLD SHIFT")) << " <-\n"
		<< "[F9] Aimbot heuristic   -> " << (sAimbotHeuristic == 0 ? "DISTANCE" : "ANGLE") << " <-\n"
		<< "[F10] Overlay           -> " << (sOverlayStatus ? "ON" : "OFF") << " <-\n"
		<< "[INSERT] Exit\n\n"
		<< line << line;
}

void Window::setGameStatus(bool b) {
	sGameStatus = b;
	Print();
}

bool Window::getGameStatus() {
	return sGameStatus;
}

void Window::setRapidFireStatus(bool b) {
	sRapidFireStatus = b;
	Print();
}

bool Window::getRapidFireStatus() {
	return sRapidFireStatus;
}

void Window::setNoRecoilStatus(bool b) {
	sNoRecoilStatus = b;
	Print();
}

bool Window::getNoRecoilStatus() {
	return sNoRecoilStatus;
}

void Window::setInfAmmoStatus(bool b) {
	sInfAmmoStatus = b;
	Print();
}

bool Window::getInfAmmoStatus() {
	return sInfAmmoStatus;
}

void Window::setLockVarsStatus(bool b) {
	sLockVarsStatus = b;
	Print();
}

bool Window::getLockVarsStatus() {
	return sLockVarsStatus;
}

void Window::setAimbotStatus(int i) {
	sAimbotStatus = i;
	Print();
}

int Window::getAimbotStatus() {
	return sAimbotStatus;
}

void Window::toggleAimbotHeuristic() {
	sAimbotHeuristic = (sAimbotHeuristic + 1) % 2;
	Print();
}

int Window::getAimbotHeuristic() {
	return sAimbotHeuristic;
}

bool Window::getOverlayStatus() {
	return sOverlayStatus;
}

void Window::setOverlayStatus(bool b) {
	sOverlayStatus = b;
	Print();
}

void Window::Reset() {
	sGameStatus = false;
	sRapidFireStatus = false;
	sNoRecoilStatus = false;
	sInfAmmoStatus = false;
	sLockVarsStatus = false; 
	sAimbotStatus = 0;
	sOverlayStatus = false;
	Print();
}