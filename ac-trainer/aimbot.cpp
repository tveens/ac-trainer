#include "aimbot.h"
#include "offsets.h"
#include "main.h"
#include <limits>
#include <Eigen\Dense>

#define PI 3.1415926535897

using namespace std;
using namespace Eigen;

Aimbot::Aimbot(Player& p) : self(p) {
}

void Aimbot::readPlayerData() {
	int numPlayers;
	uintptr_t baseAdd;
	players.clear();
	ReadProcessMemory(acHandle, (LPCVOID) pNumPlayers, &numPlayers, 4, NULL);
	ReadProcessMemory(acHandle, (LPCVOID) pBotGame, &baseAdd, 4, NULL);
	for (int i = 1; i < numPlayers; i++) {
		uintptr_t playerPtr;
		ReadProcessMemory(acHandle, (LPCVOID) (baseAdd + i * 0x4), &playerPtr, 4, NULL);
		players.push_back(Player(playerPtr));
	}
}

Player* Aimbot::getClosestEnemy() {
	float minimalDistance = numeric_limits<float>::infinity();
	Player* result = nullptr;
	int selfTeam = self.getTeam();
	for (auto &p : players) {
		if (p.getTeam() != selfTeam && p.getHealth() > 0) {
			float distance = (self.getPosition() - p.getPosition()).norm();
			if (distance < minimalDistance) {
				minimalDistance = distance;
				result = &p;
			}
		}
	}
	return result;
}

Player* Aimbot::getClosestEnemyToCrosshair() {
	double minimalDistance = numeric_limits<float>::infinity();
	Player* result = nullptr;
	int selfTeam = self.getTeam();
	for (auto &p : players) {
		if (p.getTeam() != selfTeam && p.getHealth() > 0) {
			Vector3f targetPos = p.getPosition();
			Vector3f selfPos = self.getPosition();
			Vector3f selfView = self.getViewAngle();
			float dx = targetPos[0] - selfPos[0];
			float dy = targetPos[1] - selfPos[1];
			float dz = targetPos[2] - selfPos[2];
			// yaw angle to aim at the enemy
			float yaw = atan2(dy, dx) * 180 / PI + 90;
			// absolute difference between own view angle and angle to aim at enemy
			float yawDiff = abs(selfView[0] - yaw);
			yawDiff = min(360 - yawDiff, yawDiff);

			if (yawDiff >= 90) continue;

			// distance to enemy
			float distance = sqrt(dx*dx + dy*dy);

			// distance to target orthogonal to the view direction of yourself
			float distanceXY = sin(yawDiff * PI / 180) * distance;

			float pitch = atan2(dz, distance) * 180 / PI;
			float pitchDiff = abs(selfView[1] - pitch);

			if (pitchDiff >= 90) continue;
			distance = sqrt(dx*dx + dz*dz);
			float distanceXZ = sin(pitchDiff * PI / 180) * distance;


			float realDistance = sqrt(distanceXY * distanceXY + distanceXZ * distanceXZ);
			if (realDistance < minimalDistance) {
				minimalDistance = realDistance;
				result = &p;
			}
		}
	}
	return result;
}

void Aimbot::updateAimbot(Player* target) {
	if (target == nullptr) return;

	Vector3f targetPos = target->getPosition();
	Vector3f selfPos = self.getPosition();
	Vector3f angles = Vector3f(3);
	angles[2] = (float) 0;
	float dx = targetPos[0] - selfPos[0];
	float dy = targetPos[1] - selfPos[1];
	angles[0] = (float) (atan2(dy, dx) * 180 / PI + 90);

	float distance = sqrt(dx*dx + dy*dy);
	float dz = targetPos[2] - selfPos[2];
	angles[1] = (float) (atan2(dz, distance) * 180 / PI);
	self.setViewAngle(angles);
}
