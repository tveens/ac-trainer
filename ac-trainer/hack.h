#pragma once
#include <thread>
#include "window.h"
#include "overlay.h"
#include "player.h"

class Hack {
private:
	Window win; // Console window object, GUI
	Player playerSelf; // Player object, stores address to Player Base
	Overlay ol; // ESP overlay
	std::thread lock_vars, aimbot, game_checker;
	bool stop_lock_vars = false;
	bool stop_aimbot = false;
	bool stop_game_checker = false;

	void startAimbot();
	void lockVars(); // locks player variables such as ammo and health
	void findProcess(); // finds correct game process, sets the handle and player base

public:
	Hack();
	~Hack();
	void toggleNoRecoil();
	void toggleRapidFire();
	void toggleInfAmmo();
	void toggleLockVars();
	void toggleAimbot();
	void toggleAimbotHeuristic();
	void toggleOverlay();
};