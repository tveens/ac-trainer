#pragma once
#include <vector>
#include <Eigen\Dense>

class Player {
private:
	uintptr_t playerAddress;

public:
	Player();
	Player(uintptr_t);
	void setPlayerAddress(uintptr_t);
	void setPosition(Eigen::Vector3f);
	Eigen::Vector3f getPosition();
	void setViewAngle(Eigen::Vector3f);
	Eigen::Vector3f getViewAngle();
	void setHealth(int);
	int getHealth();
	int getDeath();
	int getTeam();
	void setCustom(int, int);
	std::vector<char> getName();
};