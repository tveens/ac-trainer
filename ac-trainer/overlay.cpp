#include <windows.h>
#include <thread>
#include "overlay.h"
#include "main.h"
#include "offsets.h"

using namespace std;

Overlay::Overlay(Player &p) : self(p)  {
	// register window class
	WNDCLASSEX wc;
	winHandle = GetModuleHandle(NULL);
	ZeroMemory(&wc, sizeof(WNDCLASSEX));

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = DefWindowProc;
	wc.hInstance = winHandle;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH) RGB(0, 0, 0);
	wc.lpszClassName = WINCLASS;

	RegisterClassEx(&wc);
}

Overlay::~Overlay() {
	Stop();
	UnregisterClass(WINCLASS, winHandle);
}

void Overlay::Start() {
	stop_overlay = false;
	overlay = std::thread(&Overlay::createWindow, this);
}

void Overlay::Stop() {
	stop_overlay = true;
	if (overlay.joinable()) overlay.join();
}

void Overlay::createWindow() {
	// find and set the correct overlay dimensions
	RECT acClientSize;
	int topMenuBarOffset = 24; // offset to exclude the top bar off the game
	GetWindowRect(acHWND, &acClientSize);
	overlayWidth = acClientSize.right - acClientSize.left;
	overlayHeight = acClientSize.bottom - acClientSize.top - topMenuBarOffset;

	// create the overlay
	HWND overlayHWND = CreateWindowEx(WS_EX_TOPMOST | WS_EX_LAYERED,
		WINCLASS,
		"",
		WS_POPUP,
		acClientSize.left, acClientSize.top + topMenuBarOffset, // position
		overlayWidth, overlayHeight,
		acHWND,
		NULL,
		winHandle,
		NULL);

	SetLayeredWindowAttributes(overlayHWND, 0, 255, LWA_ALPHA); // window opaque
	SetLayeredWindowAttributes(overlayHWND, 0, 0, LWA_COLORKEY); // black pixels will be transparent

	ShowWindow(overlayHWND, SW_SHOW);
	SetFocus(acHWND);
	initD3D(overlayHWND);

	MSG msg;
	while (!stop_overlay) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			// translate keystroke messages into the right format
			TranslateMessage(&msg);

			// send the message to the WindowProc function
			DispatchMessage(&msg);
			if (msg.message == WM_QUIT)
				break;
		} else {
			readPlayerData();
			vector<box> boxes;
			calculateBoxDimensions(boxes);
			renderFrame(boxes);
		}
	}
	CleanD3D();
	DestroyWindow(overlayHWND);
}

void Overlay::initD3D(HWND hWnd) {
	d3d = Direct3DCreate9(D3D_SDK_VERSION);    // create the Direct3D interface

	D3DPRESENT_PARAMETERS d3dpp;    // create a struct to hold various device information

	ZeroMemory(&d3dpp, sizeof(d3dpp));    // clear out the struct for use
	d3dpp.Windowed = TRUE;    // program windowed, not fullscreen
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;    // discard old frames
	d3dpp.hDeviceWindow = hWnd;    // set the window to be used by Direct3D
	d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;     // set the back buffer format to 32-bit
	d3dpp.BackBufferWidth = overlayWidth;    // set the width of the buffer
	d3dpp.BackBufferHeight = overlayHeight;    // set the height of the buffer

	// create a device class using this information and the info from the d3dpp stuct
	d3d->CreateDevice(D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL,
		hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING,
		&d3dpp,
		&d3ddev);

	// create line object
	D3DXCreateLine(d3ddev, &d3dline);
	// create font object
	D3DXCreateFont(d3ddev, 15, 0, FW_NORMAL, 1, false, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, DRAFT_QUALITY, FIXED_PITCH, "Arial", &d3dFont);
}

// this is the function that cleans up Direct3D and COM
void Overlay::CleanD3D() {
	d3ddev->Release();    // close and release the 3D device
	d3d->Release();    // close and release Direct3D
	d3dline->Release();
	d3dFont->Release();
}

void Overlay::readPlayerData() {
	ReadProcessMemory(acHandle, (LPCVOID) modelviewprojectMatrix, &view.m, 64, NULL);
	int numPlayers;
	uintptr_t baseAdd;
	players.clear();
	ReadProcessMemory(acHandle, (LPCVOID) pNumPlayers, &numPlayers, 4, NULL);
	ReadProcessMemory(acHandle, (LPCVOID) pBotGame, &baseAdd, 4, NULL);
	for (int i = 1; i < numPlayers; i++) {
		uintptr_t playerPtr;
		ReadProcessMemory(acHandle, (LPCVOID) (baseAdd + i * 0x4), &playerPtr, 4, NULL);
		players.push_back(Player(playerPtr));
	}
}

void Overlay::calculateBoxDimensions(vector<box> &boxes) {
	int selfTeam = self.getTeam();
	for (auto &p : players) {
		if (p.getTeam() != selfTeam && p.getHealth() > 0) {
			Eigen::Vector3f targetPos = p.getPosition();
			float feetPos[2], headPos[2];
			bool feetPosStatus = worldToScreen(targetPos, feetPos);
			targetPos[2] += PLAYERHEIGHT;
			bool headPosStatus = worldToScreen(targetPos, headPos);
			if (feetPosStatus && headPosStatus) {
				box b;
				b.height = abs(headPos[1] - feetPos[1]);
				b.width = b.height / 2.0f;
				b.x = headPos[0] - (b.width / 2.0f);
				b.y = headPos[1];
				b.health = p.getHealth();
				b.name = p.getName();
				boxes.push_back(b);
			}
		}
	}
}

bool Overlay::worldToScreen(Eigen::Vector3f pos, float(&o)[2]) {
	float X = (view._11 * pos[0]) + (view._21 * pos[1]) + (view._31 * pos[2]) + view._41;
	float Y = (view._12 * pos[0]) + (view._22 * pos[1]) + (view._32 * pos[2]) + view._42;
	float W = (view._14 * pos[0]) + (view._24 * pos[1]) + (view._34 * pos[2]) + view._44;

	if (W < CLIPPING) return false;

	float camX = overlayWidth / 2.0f;
	float camY = overlayHeight / 2.0f;

	o[0] = camX + (camX * X / W);
	o[1] = camY - (camY * Y / W);

	return true;
}

// this is the function used to render a single frame
void Overlay::renderFrame(vector<box> &boxes) {

	// black pixels are transparent
	d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, 0, 1.0f, 0);

	d3ddev->BeginScene();    // begins the 3D scene

	for (auto &box : boxes) {
		drawRectangle(D3DCOLOR_XRGB(0, 255, 0), box);
		drawFilledRectangle(D3DCOLOR_XRGB(255, 0, 0), box, 100);
		drawFilledRectangle(D3DCOLOR_XRGB(0, 255, 0), box, box.health);
		drawName(D3DCOLOR_XRGB(255, 255, 255), box);
	}

	d3ddev->EndScene();    // ends the 3D scene

	d3ddev->Present(NULL, NULL, NULL, NULL);    // displays the created frame
}

void Overlay::drawRectangle(D3DCOLOR c, box &b) {
	D3DXVECTOR2 points[5];
	points[0] = D3DXVECTOR2(b.x, b.y);
	points[1] = D3DXVECTOR2(b.x + b.width, b.y);
	points[2] = D3DXVECTOR2(b.x + b.width, b.y + b.height);
	points[3] = D3DXVECTOR2(b.x, b.y + b.height);
	points[4] = points[0];
	d3dline->SetWidth(1);
	d3dline->Draw(points, 5, c);
}

void Overlay::drawFilledRectangle(D3DCOLOR c, box &b, int length) {
	D3DXVECTOR2 points[2];
	points[0] = D3DXVECTOR2(b.x, b.y + b.height + 25);
	points[1] = D3DXVECTOR2(b.x + length, b.y + b.height + 25);
	d3dline->SetWidth(10);
	d3dline->Draw(points, 2, c);
}

void Overlay::drawName(D3DCOLOR c, box &b) {
	RECT textBox;
	textBox.left = b.x;
	textBox.top = b.y + b.height + 5;
	d3dFont->DrawTextA(NULL, b.name.data(), 15, &textBox, DT_NOCLIP, c);
}