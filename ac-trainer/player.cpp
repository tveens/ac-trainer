#include <windows.h>
#include "player.h"
#include "offsets.h"
#include "main.h"

using namespace std;
using namespace Eigen;

Player::Player() {
	playerAddress = NULL;
}

Player::Player(uintptr_t add) {
	playerAddress = add;
}

void Player::setPlayerAddress(uintptr_t add) {
	playerAddress = add;
}

void Player::setPosition(Vector3f pos) {
	WriteProcessMemory(acHandle, (LPVOID)(playerAddress + oPlayPositionXY1), pos.data(), 12, NULL);
}

Vector3f Player::getPosition() {
	Vector3f pos = Vector3f(3);
	ReadProcessMemory(acHandle, (LPCVOID)(playerAddress + oPlayPositionXY1), pos.data(), 12, NULL);
	return pos;
}

void Player::setViewAngle(Vector3f viewAngle) {
	WriteProcessMemory(acHandle, (LPVOID)(playerAddress + oHoriViewAngle), viewAngle.data(), 12, NULL);
}

Vector3f Player::getViewAngle() {
	Vector3f viewAngle = Vector3f(3);
	ReadProcessMemory(acHandle, (LPCVOID)(playerAddress + oHoriViewAngle), viewAngle.data(), 12, NULL);
	return viewAngle;
}

void Player::setHealth(int health) {
	WriteProcessMemory(acHandle, (LPVOID)(playerAddress + oHealth), &health, 4, NULL);
}

int Player::getHealth() {
	int health;
	ReadProcessMemory(acHandle, (LPCVOID)(playerAddress + oHealth), &health, 4, NULL);
	return health;
}

int Player::getDeath() {
	int death;
	ReadProcessMemory(acHandle, (LPCVOID)(playerAddress + oDeath), &death, 4, NULL);
	return death;
}

int Player::getTeam() {
	int team;
	ReadProcessMemory(acHandle, (LPCVOID)(playerAddress + oTeam), &team, 4, NULL);
	return team;
}

void Player::setCustom(int offset, int amount) {
	WriteProcessMemory(acHandle, (LPVOID)(playerAddress + offset), &amount, 4, NULL);
}

std::vector<char> Player::getName() {
	std::vector<char> name(15);
	ReadProcessMemory(acHandle, (LPCVOID) (playerAddress + oName), name.data(), 15, NULL);
	return name;
}