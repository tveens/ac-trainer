#pragma once

class Window {
private:
	void SetWindow();
	void Print();
	bool sGameStatus;
	bool sRapidFireStatus;
	bool sNoRecoilStatus;
	bool sInfAmmoStatus;
	bool sLockVarsStatus;
	int sAimbotStatus;
	int sAimbotHeuristic;
	bool sOverlayStatus;

public:
	Window();
	void Reset();
	void setGameStatus(bool);
	bool getGameStatus();
	void setRapidFireStatus(bool);
	bool getRapidFireStatus();
	void setNoRecoilStatus(bool);
	bool getNoRecoilStatus();
	void setInfAmmoStatus(bool);
	bool getInfAmmoStatus();
	void setLockVarsStatus(bool);
	bool getLockVarsStatus();
	void setAimbotStatus(int);
	int getAimbotStatus();
	void toggleAimbotHeuristic();
	int getAimbotHeuristic();
	bool getOverlayStatus();
	void setOverlayStatus(bool);
};